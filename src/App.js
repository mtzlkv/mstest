import './App.css';
import {useState} from "react";

let listOfSomeEntities = [{
  name: 'a',
  children: [{
    name: 'a1'
  },{
    name: 'a2',
    children: [{
      name: 'a21'
    }]
  }]
},{
  name: 'b'
},{
  name: 'c',
  children: [{
    name: 'c1',
    children: [{
      name: 'c11'
    },{
      name: 'c12',
    }]
  }]
}];

function App() {
  const [flattenArray, setFlattenArray] = useState([])
  const toFlatten = (data) => {
    let result = [];
    const flatten = (data, parentName = '') => {
      data.forEach( n => {
        let fullName
        if(parentName !== ''){
          fullName = `${parentName} > ${n.name}`;
        } else fullName = n.name
        result.push(fullName);
        if (n.children) {
          flatten(n.children, fullName);
        }
      })
    }
    flatten(data);
    setFlattenArray(result);
  }

  return (
    <div className="App">
      <button onClick={() => toFlatten(listOfSomeEntities)}>Показать</button>
      <button onClick={() => setFlattenArray([])}>Очистить</button>
      <div className='contentBlock'>{flattenArray.map(i => <div key={i}>{i}</div>)}</div>
    </div>
  );
}
export default App;
